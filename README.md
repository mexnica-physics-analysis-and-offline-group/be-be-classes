* BeBe files should be located at the mpdroot/bmd directory.

* BeBe should be added to the mpdloadlibs.C file located at .../mpdroot/macro/mpd/ with the code line:   gSystem->Load("libbmd");

* BeBe geometry should be included in the geometry_stage macro withe next code lines:  
 
    
* FairDetector *Bmd = new BmdDetector("BMD",kTRUE );
    Bmd->SetGeometryFileName("bbc_hex_5cm_NDetScin_v1.root");
    fRun->AddModule(Bmd);
 
